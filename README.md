# Enrique

Enrique provides the p2p infrastructure and management
of the runtime engines. It implements the roles and
responsibilities of the network operating system.

Enrique enables user's to view the relationships between
servers (resource providers).
