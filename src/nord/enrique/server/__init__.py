"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

initial work on p2p based upon:
https://github.com/ethereum/pydevp2p/blob/develop/devp2p/examples/full_app.py

Needed to remove the pyelliptic installed by pip when I ran
pip install devp2p

and then install:
pip install https://github.com/mfranciszkiewicz/pyelliptic/archive/1.5.10.tar.gz#egg=pyelliptic

to resolve the exception:
AttributeError: /lib/x86_64-linux-gnu/libcrypto.so.1.1: undefined symbol: ECDH_OpenSSL


"""

import sys
import random
from devp2p.app import BaseApp
from devp2p.protocol import BaseProtocol
from devp2p.service import WiredService
from devp2p.crypto import privtopub as privtopub_raw, sha3
from devp2p.utils import colors, COLOR_END
from devp2p import app_helper
import rlp
from rlp.utils import encode_hex, decode_hex, is_integer
import gevent
import nord.sigurd.utils.log as log


class Token(rlp.Serializable):

    "Object with the information to update a decentralized counter"
    fields = [
        ('counter', rlp.sedes.big_endian_int),
        ('sender', rlp.sedes.binary)
    ]

    def __init__(self, counter=0, sender=''):
        assert is_integer(counter)
        assert isinstance(sender, bytes)
        super(Token, self).__init__(counter, sender)

    @property
    def hash(self):
        return sha3(rlp.encode(self))

    def __repr__(self):
        try:
            return '<%s(counter=%d hash=%s)>' % (self.__class__.__name__, self.counter,
                                                 encode_hex(self.hash)[:4])
        except Exception:
            return '<%s>' % (self.__class__.__name__)


class ExampleProtocol(BaseProtocol):
    protocol_id = 1
    network_id = 0
    max_cmd_id = 1  # Actually max id is 0, but 0 is the special value.
    name = b'example'
    version = 1

    def __init__(self, peer, service):
        # required by P2PProtocol
        self.config = peer.config
        BaseProtocol.__init__(self, peer, service)

    class token(BaseProtocol.command):

        """
        message sending a token and a nonce
        """
        cmd_id = 0

        structure = [
            ('token', Token)
        ]


class DuplicatesFilter(object):

    def __init__(self, max_items=1024):
        self.max_items = max_items
        self.filter = list()

    def update(self, data):
        "returns True if unknown"
        if data not in self.filter:
            self.filter.append(data)
            if len(self.filter) > self.max_items:
                self.filter.pop(0)
            return True
        else:
            self.filter.append(self.filter.pop(0))
            return False

    def __contains__(self, v):
        return v in self.filter


class ExampleService(WiredService):

    # required by BaseService
    name = 'exampleservice'
    default_config = dict(example=dict(num_participants=1))

    # required by WiredService
    wire_protocol = ExampleProtocol  # create for each peer

    def __init__(self, app):
        self.config = app.config
        log.note("Initializing Service...")
        self.address = privtopub_raw(decode_hex(self.config['node']['privkey_hex']))
        super(ExampleService, self).__init__(app)
        log.warn("Service initialized")

    def start(self):
        log.crit("Starting Service")
        super(ExampleService, self).start()

    def log(self, text, **kargs):
        node_num = self.config['node_num']
        msg = ' '.join([
            colors[node_num % len(colors)],
            "NODE%d" % node_num,
            text,
            (' %r' % kargs if kargs else ''),
            COLOR_END])
        log.debug(msg)

    def broadcast(self, obj, origin=None):
        fmap = {Token: 'token'}
        log.info(f'broadcasting obj: {obj}')
        bcast = self.app.services.peermanager.broadcast
        bcast(ExampleProtocol, fmap[type(obj)], args=(obj,),
              exclude_peers=[origin.peer] if origin else [])

    def on_wire_protocol_stop(self, proto):
        assert isinstance(proto, self.wire_protocol)
        log.info('----------------------------------')
        log.info(f'on_wire_protocol_stop {proto}')

    # application logic

    def on_wire_protocol_start(self, proto):
        log.info('----------------------------------')
        log.info(f'on_wire_protocol_start proto: {proto} peers: {self.app.services.peermanager.peers}')
        assert isinstance(proto, self.wire_protocol)
        # register callbacks
        proto.receive_token_callbacks.append(self.on_receive_token)
        self.send_token()

    def on_receive_token(self, proto, token):
        assert isinstance(token, Token)
        assert isinstance(proto, self.wire_protocol)
        log.info('----------------------------------')
        log.info(f'on_receive token {token} via {proto}')
        self.send_token()

    def send_token(self):
        gevent.sleep(random.random())
        token = Token(counter=random.randint(0, 1024), sender=self.address)
        log.info('----------------------------------')
        log.info(f'sending token {token}')
        self.broadcast(token)


def do_some_talking(app):
    log.verb("No need to do anything here.")


class ExampleApp(BaseApp):
    client_name = 'exampleapp'
    version = '0.1'
    client_version = '%s/%s/%s' % (version, sys.platform,
                                   'py%d.%d.%d' % sys.version_info[:3])
    client_version_string = '%s/v%s' % (client_name, client_version)
    default_config = dict(BaseApp.default_config)
    default_config['client_version_string'] = client_version_string
    default_config['post_app_start_callback'] = do_some_talking


def start():
    """
    Start the server instance, and handle requests once ready.
    """
    app_helper.run(ExampleApp, ExampleService)
